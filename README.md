# Business Platform API Documentation

### This repository holds the API documentation for the services supported by Enterprise Business Platform.
<br />

**Please follow this structure to add new APIs**


1. **src/parameters**: This folder contains all the header, query or path parameters to be used by APIs.
2. **src/schemas**: This folder conatains all the model schemas.
3. **src/resources**: This folder contains all the resource definitions with reference to parameters, reponse types and other required entities.
4. **src/openapi.yaml**: Add all the reference to APIs in this file.

Please complete these steps before commiting the code:

Run this to generate the consolidated yaml file in _build repository:
```
 npm run build
```

Run this command to view documentation locally at http://127.0.0.1:8080:
```
 npm run preview
```

Run this command to create index.html for static hosting:
```
 npm run html
```

